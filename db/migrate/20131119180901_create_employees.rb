class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :last_name
      t.string :first_name
      t.string :position
      t.string :department
      t.string :phone_num
      t.string :email

      t.timestamps
    end
  end
end
