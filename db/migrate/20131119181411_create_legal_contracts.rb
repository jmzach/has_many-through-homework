class CreateLegalContracts < ActiveRecord::Migration
  def change
    create_table :legal_contracts do |t|
      t.string :contract_status
      t.text :contract_description
      t.integer :sponsor_id

      t.timestamps
    end
  end
end
