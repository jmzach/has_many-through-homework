class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :last_name
      t.string :first_name
      t.integer :sponsor_id
      t.integer :phone_num
      t.string :email
      t.string :location

      t.timestamps
    end
  end
end
