class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.string :sponsor_name
      t.integer :employee_id
      t.string :sponsor_type
      t.float :sponsor_amount

      t.timestamps
    end
  end
end
