class Contact < ActiveRecord::Base
  belongs_to :sponsor
  attr_accessible :email, :first_name, :last_name, :location, :phone_num, :sponsor_id, :sponsor
end
