class Sponsor < ActiveRecord::Base
  belongs_to :employee
  has_many :contacts
  has_many :legal_contracts
  attr_accessible :employee_id, :sponsor_amount, :sponsor_name, :sponsor_type, :employee
end
