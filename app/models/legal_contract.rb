class LegalContract < ActiveRecord::Base
  belongs_to :sponsor
  attr_accessible :contract_description, :contract_status, :sponsor_id, :sponsor
end
