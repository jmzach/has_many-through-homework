class Employee < ActiveRecord::Base
  has_many :sponsors
  has_many :contacts, through: :sponsors
  has_many :legal_contracts, through: :sponsors
  attr_accessible :department, :email, :first_name, :last_name, :phone_num, :position
end
